// react imports
import React from "react";
import { Link } from "react-router-dom";

import "./DecksList.css";

// bootstrap imports
import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";

// other imports
import CreateDeckModal from "./CreateDeckModal";
import { useGetMyDecksQuery } from "../../store/myCardsApi";
import Loading from "../ui/Loading";
import { getAverageCmc, getDominantColors } from "./DeckDetail";
import ParseSymbolsAndLineBreaks from "../card-details/ParseSymbolsAndLineBreaks";

export function getDeckCardQuantity(deck) {
  let quantity = 0;
  if (deck.cards === undefined || deck.cards.length === 0) {
    return quantity;
  }
  for (let card of deck.cards) {
    quantity += card.quantity;
  }
  return quantity;
}

function DecksList() {
  // const [usersDecks, setUsersDecks] = useState([]);
  const {
    data: usersDecks,
    error: decksError,
    isLoading: decksIsLoading,
  } = useGetMyDecksQuery();

  if (decksIsLoading || decksError) {
    return <Loading />;
  }

  return (
    <React.Fragment>
      <Container style={{ marginTop: "30px", marginBottom: "30px" }}>
        <CreateDeckModal />
      </Container>

      <Container>
        <div className="custom-scrollbar table-wrapper-scroll-y">
          <Table striped bordered>
            <thead>
              <tr>
                <th>Deck Name</th>
                <th>Cards</th>
                <th>Colors</th>
                <th>Average Mana Cost</th>
                <th>Price (USD)</th>
              </tr>
            </thead>
            <tbody>
              {usersDecks.decks.map((deck) => {
                const colors = getDominantColors(deck);
                const colorsMana =
                  colors.length === 2
                    ? `{${colors[0]}}{${colors[1]}}`
                    : colors.length === 1
                    ? `{${colors[0]}}`
                    : "";
                return (
                  <React.Fragment key={deck.id}>
                    <tr
                      data-toggle="collapse"
                      data-target="#collapseFour"
                      aria-expanded="true"
                      aria-controls="collapseFour"
                      className=""
                    >
                      <th scope="row">
                        <Link to={`/deck/${deck.id}/`}>
                          <ParseSymbolsAndLineBreaks string={deck.name} />
                        </Link>
                      </th>
                      <td>{getDeckCardQuantity(deck)}</td>
                      <td>
                        <ParseSymbolsAndLineBreaks string={colorsMana} />
                      </td>
                      <td>{getAverageCmc(deck)}</td>
                      <td>$TBD</td>
                    </tr>
                  </React.Fragment>
                );
              })}
            </tbody>
          </Table>
        </div>
      </Container>
    </React.Fragment>
  );
}

export default DecksList;
